History
-------
In reversed order.

* User Controller: getTemporaryProfileImage.
* User Controller: getProfileImage.
* User Controller: updateProfileImage.
* User Controller: deleteUser.
* User Controller: resetPassword.
* User Controller: getUser, getAllUsers.
* User Controller: updateUser.
* User Controller: addNewUser.
* User Service: update: UserServiceImpl::resetPassword,updateProfileImage.
* User Service: update: UserServiceImpl::updateUser,deleteUser.
* User Service: update: UserServiceImpl::addNewUser.
* User Service: create user folder, add FileConstants.
* User Service: update interface UserService.
* Email notification: add email properties to application.yml.
* Email notification: EmailService::sendNewPasswordEmail.
* Email notification: EmailService::createEmail.
* Email notification: EmailService::getEmailSession.
* Email notification: EmailConstants.
* Brute force attack: validate user login - UserServiceImpl.
* Brute force attack: AuthenticationSuccessListener.
* Brute force attack: AuthenticationFailureListener.
* Brute force attack: LoginAttemptService.
* Logging in and generating JWT
* Implement User registration: UserServiceImpl, UserController.
* Add Roles and Authorities.
* ExceptionHandling - to handle all exceptions.
* EmailExistsException, EmailNotFoundException, UsernameExistsException, UserNotFoundException.
* UserController (for test).
* SecurityConfiguration.
* UserService, UserServiceImpl implements UserService, UserDetailsService.
* UserRepository.
* JwtAccessDeniedHandler::handle.
* JwtAuthenticationEntryPoint::commence.
* JwtAuthorizationFilter::doFilterInternal.
* JwtProvider::isTokenValid.
* JwtProvider::getAuthentication.
* JwtProvider::getAuthorities.
* JwtProvider::generateJwtToken.
* SecurityConstants.
* Add yaml properties for h2 and mysql.
* Use mysql_db_users.sql script to create DB and user.
* UserPrincipal implements UserDetails.
* HttpResponse - A uniform way of sending a response back to the client.
* User entity.
