package learn.springang.umsportal.constants;

public class FileConstants {
    public static final String USER_IMAGE_PATH = "/user/image/";
    public static final String JPG_EXTENSION = "jpg";
    public static final String PNG_EXTENSION = "png";
    public static final String USER_FOLDER = System.getProperty("user.home") + "/apps/umsportal/user/";
    public static final String DIRECTORY_CREATED_FORMAT = "Created directory for: {}";
    public static final String DEFAULT_USER_IMAGE_PATH = "/user/image/profile/";
    public static final String FILE_SAVED_IN_FILE_SYSTEM_FORMAT = "Saved file {} in file system as {}.";
    public static final String DOT = ".";
    public static final String FORWARD_SLASH = "/";
    public static final String TEMP_PROFILE_IMAGE_BASE_URL = "https://robohash.org/";
}
