package learn.springang.umsportal.constants;

public class UserServiceImplConstants {

    public static final String NO_USER_FOUND_BY_USERNAME = "No user found by username ";
    public static final String USERNAME_ALREADY_EXISTS = "Username already exists";
    public static final String EMAIL_ALREADY_EXISTS = "Email already exists";
    public static final String NO_USER_FOUND_BY_EMAIL = "No user found by email";
    public static final String NOT_AN_IMAGE_FILE = " is not an image file. Please upload an image.";
    public static final String EMPTY = "";
}
