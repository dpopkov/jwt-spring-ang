package learn.springang.umsportal.controllers;

import learn.springang.umsportal.domain.HttpResponse;
import learn.springang.umsportal.domain.User;
import learn.springang.umsportal.domain.UserPrincipal;
import learn.springang.umsportal.exceptions.ExceptionHandling;
import learn.springang.umsportal.exceptions.domain.*;
import learn.springang.umsportal.services.UserService;
import learn.springang.umsportal.utils.JwtProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static learn.springang.umsportal.constants.FileConstants.TEMP_PROFILE_IMAGE_BASE_URL;
import static learn.springang.umsportal.constants.FileConstants.USER_FOLDER;
import static learn.springang.umsportal.constants.SecurityConstants.JWT_HEADER;
import static learn.springang.umsportal.constants.UserServiceImplConstants.NO_USER_FOUND_BY_USERNAME;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;

@RestController
@RequestMapping(path = {"/", "/user"})
public class UserController extends ExceptionHandling {

    private static final String EMAIL_WITH_A_NEW_PASSWORD_WAS_SENT = "An email with a new password was sent to: ";
    private static final String USER_DELETED_SUCCESSFULLY = "User deleted successfully";

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;

    public UserController(UserService userService, AuthenticationManager authenticationManager, JwtProvider jwtProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestBody User user)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException, MessagingException {
        User registered = userService.register(user.getFirstName(), user.getLastName(), user.getUsername(), user.getEmail());
        return new ResponseEntity<>(registered, HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody User user) {
        authenticate(user.getUsername(), user.getPassword());
        User loginUser = userService.findByUsername(user.getUsername()).orElseThrow();
        UserPrincipal userPrincipal = new UserPrincipal(loginUser);
        HttpHeaders jwtHeader = getJwtHeader(userPrincipal);
        return new ResponseEntity<>(loginUser, jwtHeader, HttpStatus.OK);
    }

    private void authenticate(String username, String password) {
        Authentication authenticate = new UsernamePasswordAuthenticationToken(username, password);
        authenticationManager.authenticate(authenticate);
    }

    private HttpHeaders getJwtHeader(UserPrincipal userPrincipal) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_HEADER, jwtProvider.generateJwtToken(userPrincipal));
        return headers;
    }

    @PostMapping("/add")
    public ResponseEntity<User> addNewUser(@RequestParam("firstName") String firstName,
                                           @RequestParam("lastName") String lastName,
                                           @RequestParam("username") String username,
                                           @RequestParam("email") String email,
                                           @RequestParam("role") String role,
                                           @RequestParam("notLocked") String notLocked,
                                           @RequestParam("active") String active,
                                           @RequestParam(value = "profileImage", required = false) MultipartFile image)
            throws UserNotFoundException, UsernameExistsException, MessagingException, EmailExistsException,
            IOException, NotAnImageFileException {
        User user = userService.addNewUser(firstName, lastName, username, email, role,
                Boolean.parseBoolean(notLocked), Boolean.parseBoolean(active), image);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<User> updateUser(@RequestParam("currentUsername") String currentUsername,
                                           @RequestParam("firstName") String newFirstName,
                                           @RequestParam("lastName") String newLastName,
                                           @RequestParam("username") String newUsername,
                                           @RequestParam("email") String newEmail,
                                           @RequestParam("role") String newRole,
                                           @RequestParam("notLocked") String newNotLocked,
                                           @RequestParam("active") String newActive,
                                           @RequestParam(value = "profileImage", required = false) MultipartFile image)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException,
            IOException, NotAnImageFileException {
        User updatedUser = userService.updateUser(currentUsername, newFirstName, newLastName, newUsername, newEmail, newRole,
                Boolean.parseBoolean(newNotLocked), Boolean.parseBoolean(newActive), image);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    @GetMapping("/find/{username}")
    public ResponseEntity<User> getUser(@PathVariable String username) throws UserNotFoundException {
        User user = userService.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(NO_USER_FOUND_BY_USERNAME)); // better to throw in service
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.getUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PutMapping("/resetPassword/{email}")
    public ResponseEntity<HttpResponse> resetPassword(@PathVariable String email) throws EmailNotFoundException, MessagingException {
        userService.resetPassword(email);
        return response(HttpStatus.OK, EMAIL_WITH_A_NEW_PASSWORD_WAS_SENT + email);
    }

    @DeleteMapping("/delete/{username}")
    @PreAuthorize("hasAuthority('user:delete')")
    public ResponseEntity<HttpResponse> deleteUser(@PathVariable String username) throws IOException {
        userService.deleteUser(username);
        return response(HttpStatus.OK, USER_DELETED_SUCCESSFULLY);
    }

    @PutMapping("/updateProfileImage")
    public ResponseEntity<User> updateProfileImage(@RequestParam String username,
                                           @RequestParam(value = "profileImage") MultipartFile image)
            throws UserNotFoundException, IOException, NotAnImageFileException {
        User updatedUser = userService.updateProfileImage(username, image);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    @GetMapping(path = "/image/{username}/{filename}", produces = IMAGE_JPEG_VALUE)
    public byte[] getProfileImage(@PathVariable String username, @PathVariable String filename) throws IOException {
        return Files.readAllBytes(Paths.get(USER_FOLDER, username, filename));
    }

    @GetMapping(path = "/image/profile/{username}", produces = IMAGE_JPEG_VALUE)
    public byte[] getTemporaryProfileImage(@PathVariable String username) throws IOException {
        URL url = new URL(TEMP_PROFILE_IMAGE_BASE_URL + username);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (InputStream in = url.openStream()) {
            int numRead;
            byte[] buffer = new byte[1024];
            while ((numRead = in.read(buffer)) != -1) {
                outputStream.write(buffer, 0, numRead);
            }
        }
        return outputStream.toByteArray();
    }

    private ResponseEntity<HttpResponse> response(HttpStatus status, String message) {
        HttpResponse body = new HttpResponse(status.value(), status, status.getReasonPhrase(), message);
        return new ResponseEntity<>(body, status);
    }
}
