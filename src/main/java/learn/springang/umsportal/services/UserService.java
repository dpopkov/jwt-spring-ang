package learn.springang.umsportal.services;

import learn.springang.umsportal.domain.User;
import learn.springang.umsportal.exceptions.domain.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    /**
     * Creates a new user when you are not logged in into the application.
     * @return new User object
     */
    User register(String firstName, String lastName, String username, String email)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException, MessagingException;

    List<User> getUsers();

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    /**
     * Adds a new user when you are already logged in into the application.
     * @return new User object
     */
    User addNewUser(String firstName, String lastName, String username, String userEmail, String role,
                    boolean notLocked, boolean active, MultipartFile profileImage)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException, MessagingException, IOException, NotAnImageFileException;

    User updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername, String newEmail,
                    String role, boolean notLocked, boolean active, MultipartFile profileImage) throws UserNotFoundException, UsernameExistsException, EmailExistsException, IOException, NotAnImageFileException;

    void deleteUser(String username) throws IOException;

    void resetPassword(String email) throws EmailNotFoundException, MessagingException;

    User updateProfileImage(String username, MultipartFile profileImage) throws UserNotFoundException, IOException, NotAnImageFileException;
}
