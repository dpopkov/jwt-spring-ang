package learn.springang.umsportal.services.impl;

import learn.springang.umsportal.domain.User;
import learn.springang.umsportal.domain.UserPrincipal;
import learn.springang.umsportal.enumerations.Role;
import learn.springang.umsportal.exceptions.domain.*;
import learn.springang.umsportal.repositories.UserRepository;
import learn.springang.umsportal.services.EmailService;
import learn.springang.umsportal.services.LoginAttemptService;
import learn.springang.umsportal.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

import static learn.springang.umsportal.constants.FileConstants.*;
import static learn.springang.umsportal.constants.UserServiceImplConstants.*;

@Slf4j
@Service
@Transactional
@Qualifier("userDetailsService")
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final LoginAttemptService loginAttemptService;
    private final EmailService emailService;
    @Value("${app.email.notification}")
    private boolean useUserNotificationByEmail;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder,
                           LoginAttemptService loginAttemptService, EmailService emailService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.loginAttemptService = loginAttemptService;
        this.emailService = emailService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> byUsername = userRepository.findByUsername(username);
        if (byUsername.isEmpty()) {
            log.error("User not found by username {}", username);
            throw new UsernameNotFoundException("User not found by username");
        }
        User user = byUsername.get();
        validateLoginAttempt(user);
        user.setLastLoginDateDisplay(user.getLastLoginDate());
        user.setLastLoginDate(new Date());
        userRepository.save(user);
        UserDetails userDetails = new UserPrincipal(user);
        log.info("User found by username {}", userDetails.getUsername());
        return userDetails;
    }

    private void validateLoginAttempt(User user) {
        if (user.isNotLocked()) {
            if (loginAttemptService.hasExceededMaxAttempts(user.getUsername())) {
                user.setNotLocked(false);
            }
        } else {
            loginAttemptService.evictUserFromLoginAttemptCache(user.getUsername());
        }
    }

    @Override
    public User register(String firstName, String lastName, String username, String userEmail)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException, MessagingException {
        validateNewUsernameAndEmail(EMPTY, username, userEmail);
        String password = generatePassword();
        User user = User.builder()
                .userId(generateUserId())
                .firstName(firstName)
                .lastName(lastName)
                .username(username)
                .password(encodePassword(password))
                .email(userEmail)
                .profileImageUrl(getTemporaryProfileImageUrl(username))
                .joinDate(new Date())
                .role(Role.ROLE_USER.name())
                .authorities(Role.ROLE_USER.getAuthorities())
                .active(true)
                .notLocked(true)
                .build();
        userRepository.save(user);
        log.trace("Registered New User password {}", password);
        if (useUserNotificationByEmail) {
            emailService.sendNewPasswordEmail(user.getFirstName(), password, userEmail);
        }
        return user;
    }

    @Override
    public User addNewUser(String firstName, String lastName, String username, String userEmail, String role,
                           boolean notLocked, boolean active, MultipartFile profileImage)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException, MessagingException, IOException, NotAnImageFileException {
        validateNewUsernameAndEmail(EMPTY, username, userEmail);
        String password = generatePassword();
        Role roleEnum = getRoleEnum(role);
        User user = User.builder()
                .userId(generateUserId())
                .firstName(firstName)
                .lastName(lastName)
                .username(username)
                .password(encodePassword(password))
                .email(userEmail)
                .profileImageUrl(getTemporaryProfileImageUrl(username)) // todo: check and fix if necessary
                .joinDate(new Date())
                .role(roleEnum.name())
                .authorities(roleEnum.getAuthorities())
                .active(active)
                .notLocked(notLocked)
                .build();
        userRepository.save(user);
        saveProfileImage(user, profileImage);
        log.trace("Added New User password {}", password);
        if (useUserNotificationByEmail) {
            emailService.sendNewPasswordEmail(user.getFirstName(), password, userEmail);
        }
        return user;
    }

    private String generateUserId() {
        return RandomStringUtils.randomNumeric(10);
    }

    private String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(10);
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    private Optional<User> validateNewUsernameAndEmail(String currentUsername, String newUsername, String email)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException {
        Optional<User> byUsername = findByUsername(newUsername);
        if (StringUtils.hasText(currentUsername)) {
            User currentUser = findByUsername(currentUsername)
                    .orElseThrow(() -> new UserNotFoundException(NO_USER_FOUND_BY_USERNAME + currentUsername));
            if (byUsername.isPresent() && !currentUser.getId().equals(byUsername.get().getId())) {
                throw new UsernameExistsException(USERNAME_ALREADY_EXISTS);
            }
            Optional<User> byNewEmail = findByEmail(email);
            if (byNewEmail.isPresent() && !currentUser.getId().equals(byNewEmail.get().getId())) {
                throw new EmailExistsException(EMAIL_ALREADY_EXISTS);
            }
            return Optional.of(currentUser);
        } else {
            if (byUsername.isPresent()) {
                throw new UsernameExistsException(USERNAME_ALREADY_EXISTS);
            }
            if (findByEmail(email).isPresent()) {
                throw new EmailExistsException(EMAIL_ALREADY_EXISTS);
            }
            return Optional.empty();
        }
    }

    private String getTemporaryProfileImageUrl(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(DEFAULT_USER_IMAGE_PATH + username)
                .toUriString();
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername,
                           String newEmail, String role, boolean notLocked, boolean active, MultipartFile profileImage)
            throws UserNotFoundException, UsernameExistsException, EmailExistsException, IOException, NotAnImageFileException {
        User currentUser = validateNewUsernameAndEmail(currentUsername, newUsername, newEmail).orElseThrow();
        currentUser.setFirstName(newFirstName);
        currentUser.setLastName(newLastName);
        currentUser.setUsername(newUsername);
        currentUser.setEmail(newEmail);
        Role roleEnum = getRoleEnum(role);
        currentUser.setRole(roleEnum.name());
        currentUser.setAuthorities(roleEnum.getAuthorities());
        currentUser.setNotLocked(notLocked);
        currentUser.setActive(active);
        userRepository.save(currentUser);
        if (profileImage != null && !profileImage.isEmpty()) {
            saveProfileImage(currentUser, profileImage);
        }
        log.trace("Updated User {}", currentUser.getUsername());
        return currentUser;
    }

    private Role getRoleEnum(String role) {
        String name = role.toUpperCase();
        if (!name.startsWith("ROLE_")) {
            name = "ROLE_" + name;
        }
        return Role.valueOf(name);
    }

    @Override
    public void deleteUser(String username) throws IOException {
        userRepository.deleteByUsername(username);
        deleteUserImage(username);
        log.trace("User deleted by username {}", username);
    }

    private void deleteUserImage(String username) throws IOException {
        Path userFolder = Paths.get(USER_FOLDER, username).toAbsolutePath().normalize();
        FileUtils.deleteDirectory(userFolder.toFile());
    }

    @Override
    public void resetPassword(String email) throws EmailNotFoundException, MessagingException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EmailNotFoundException(NO_USER_FOUND_BY_EMAIL));
        String password = generatePassword();
        user.setPassword(encodePassword(password));
        userRepository.save(user);
        log.trace("Password reset to {}", password);
        if (useUserNotificationByEmail) {
            emailService.sendNewPasswordEmail(user.getFirstName(), password, user.getEmail());
        }
    }

    @Override
    public User updateProfileImage(String username, MultipartFile profileImage)
            throws UserNotFoundException, IOException, NotAnImageFileException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(NO_USER_FOUND_BY_USERNAME));
        saveProfileImage(user, profileImage);
        return user;
    }

    private void saveProfileImage(User user, MultipartFile profileImage) throws IOException, NotAnImageFileException {
        if (profileImage != null && !profileImage.isEmpty()) {
            String extension = getExtensionIfImage(Objects.requireNonNull(profileImage.getContentType()));
            if (extension == null) {
                throw new NotAnImageFileException(
                        profileImage.getOriginalFilename() + NOT_AN_IMAGE_FILE);
            }
            final String userImageFilename = user.getUsername() + DOT + extension;
            final Path userFolder = ensureFolderForImage(user);
            final Path imagePath = userFolder.resolve(userImageFilename);
            // Replace with new image
            Files.deleteIfExists(imagePath);
            Files.copy(profileImage.getInputStream(), imagePath, StandardCopyOption.REPLACE_EXISTING);
            user.setProfileImageUrl(profileImageUrl(user.getUsername(), extension));
            userRepository.save(user);
            log.info(FILE_SAVED_IN_FILE_SYSTEM_FORMAT, profileImage.getOriginalFilename(), userImageFilename);
        }
    }

    private String getExtensionIfImage(String contentType) {
        switch (contentType) {
            case MediaType.IMAGE_JPEG_VALUE:
                return JPG_EXTENSION;
            case MediaType.IMAGE_PNG_VALUE:
                return PNG_EXTENSION;
            default:
                return null;
        }
    }

    private Path ensureFolderForImage(User user) throws IOException {
        Path folder = Paths.get(USER_FOLDER, user.getUsername()).toAbsolutePath().normalize();
        if (!Files.exists(folder)) {
            Files.createDirectories(folder);
            log.info(DIRECTORY_CREATED_FORMAT, folder);
        }
        return folder;
    }

    private String profileImageUrl(String username, String extension) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(USER_IMAGE_PATH + username + FORWARD_SLASH + username + DOT + extension)
                .toUriString();
    }
}
